import { Router } from "express";
import { signIn, signUp } from "../controllers/auth.controller";
import { checkUserExist } from "../middlewares";

const router = Router()

router.post("/signup",checkUserExist,signUp)
router.post("/signin", signIn)

export default router;