import { Router } from "express";
import { createUser, getUser, getUsers } from "../controllers/users.controller";
import { checkRolesExist, checkUserExist, isAdmin, verifyToken } from "../middlewares";

const router = Router()

router.get("/", getUsers);
router.post("/",[verifyToken,isAdmin,checkRolesExist,checkUserExist], createUser);
router.get("/:id", getUser);

export default router;