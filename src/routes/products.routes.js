import { Router } from "express";
import { createProduct, deleteProduct, getProduct, getProducts, updateProduct } from "../controllers/products.controller";
import { verifyToken,isAdmin,isModerator } from "../middlewares";


const router = Router()


router.get("/",getProducts)
router.post("/",[verifyToken,isAdmin],createProduct)
router.get("/:id",getProduct)
router.put("/:id",[verifyToken,isModerator],updateProduct)
router.delete("/:id",[verifyToken,isAdmin],deleteProduct)



export default router;