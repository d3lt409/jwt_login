import app from "./app"
import { db } from "./database"
import { createRoles } from "./libs/initialSetup"

db.sequelize.sync()
createRoles()
app.listen(3000,()=>{
    console.log("server on port", 3000)
})