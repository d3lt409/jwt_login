import { Role } from "../models/Role";

export const createRoles = async () => {
    try {
        const roles = await Promise.all([
            Role.create({name:"Admin"},{ignoreDuplicates:true}),
            Role.create({name:"Moderator"},{ignoreDuplicates:true}),
            Role.create({name:"User"},{ignoreDuplicates:true})
        ])
    } catch (error) {
        console.error(error)
    }
    
}

