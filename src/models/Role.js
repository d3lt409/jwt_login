import { db } from "../database";
import { DataTypes } from "sequelize";

export const Role = db.sequelize.define("Role",{
    id:{
        autoIncrement:true,
        primaryKey:true,
        unique:true,
        type: DataTypes.INTEGER
    },
    name:{
        type: DataTypes.STRING,
        unique:true,
        allowNull:false
    }
},{
    timestamps:false
})