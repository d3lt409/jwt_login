import { db } from "../database";
import { DataTypes } from "sequelize";

export const Product = db.sequelize.define("Product",{
    id:{
        autoIncrement:true,
        primaryKey:true,
        unique:true,
        type: DataTypes.INTEGER
    },
    name:{
        type: DataTypes.STRING,
        allowNull:false
    },
    category:{
        type: DataTypes.STRING,
        allowNull:false
    },
    price:{
        type: DataTypes.DECIMAL,
        allowNull:false
    },
    imgUrl:{
        type: DataTypes.STRING,
        allowNull:false
    }
},{
    timestamps:true
})