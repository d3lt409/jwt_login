import { db } from "../database";
import { DataTypes } from "sequelize";
import { Role } from "./Role";
import bcrypt from "bcryptjs";

const User = db.sequelize.define("User",{
    id:{
        autoIncrement:true,
        primaryKey:true,
        unique:true,
        type: DataTypes.INTEGER
    },
    username:{
        type: DataTypes.STRING,
        unique:true,
        allowNull:false
    },
    email:{
        type: DataTypes.STRING,
        unique:true,
        allowNull:false
    },
    password:{
        type: DataTypes.STRING,
        allowNull:false
    }
},{
    timestamps:true,
    hooks:{
        beforeCreate: async (user) => {
            if (user.password){
                const saltos = await bcrypt.genSaltSync();
                user.password = bcrypt.hashSync(user.password,saltos)
            }
        },
        beforeUpdate: async (user) => {
            if (user.password){
                const saltos = await bcrypt.genSaltSync();
                user.password = bcrypt.hashSync(user.password,saltos)
            }
        }
    }
})

User.belongsToMany(Role,{through: 'User_Roles'})
Role.belongsToMany(User,{through: 'User_Roles'})

User.prototype.validPassword = async (password,hash) => {
    return await bcrypt.compare(password,hash)
}

export default User;