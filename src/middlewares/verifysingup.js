import { Role } from "../models/Role";
import User from "../models/User";

export const checkRolesExist = async (req,res,next) => {
    if (req.body.roles){
        const roles = (await Role.findAll()).map(role => role.name);
        for (let i = 0; i < req.body.roles.length; i++) {
            const role = req.body.roles[i];
            if (!roles.includes(role)){
                return res.status(400).json({
                    msg:`Role ${role} does not exist `
                })
            }
        }
    }
    next()
}

export const checkUserExist = async (req,res,next) => {
    const username = await User.findOne({where:{username:req.body.username}})
    if (username) return res.status(400).json({msg:"The username already exist"})
    const email = await User.findOne({where:{email:req.body.email}})
    if (email) return res.status(400).json({msg:"The email already exist"})

    next()

}