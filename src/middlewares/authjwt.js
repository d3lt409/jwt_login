import jwt from "jsonwebtoken"
import config from "../config"
import { Role } from "../models/Role"
import User from "../models/User"

export const verifyToken = async (req,res, next) => {
    try {
        const token = req.headers['x-access-token']
        if (!token) return res.status(403).json({msg:"No token provided"})

        const decode = jwt.verify(token,config.SECRET)
        const user =await User.findByPk(decode.id,{include:Role});
        if (!user) return res.status(404).json({msg:"No user found"})
        req.user = user
        next()
    } catch (error) {
        return res.status(404).json({msg:error})
    }
}

export const isAdmin = (req,res,next) => {

    const roles = req.user.Roles
    
    for (let i = 0; i < roles.length; i++) {
        const role = roles[i];
        if (role.name === 'Admin'){
            next();
            return;
        }
        
    }
    return res.status(403).json({msg:"Require Admin role"})
}

export const isModerator = async (req,res,next) => {
    const roles = req.user.Roles
    
    for (let i = 0; i < roles.length; i++) {
        const role = roles[i];
        if (role.name === 'Admin' || role.name === 'Moderator'){
            next();
            return;
        }
        
    }
    return res.status(403).json({msg:"Require Moderator or Admin role"})
}