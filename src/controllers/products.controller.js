import { Product } from "../models/Product"


export const createProduct = async (req,res) => {
    try {
        const product = await Product.create(req.body)
        if (product) return res.status(201).json({msg:"Producto creado",product})
        return res.status(401).json({msg:"algún error ocurrió mientras se creaba"})
    } catch (error) {
        return res.status(500).json({msg:error})
    }
    
}

export const getProducts = async (req,res) => {
    try {
        const products = await Product.findAll();
        if (products.length > 0){
            return res.status(200).json(products)
        }
        return res.status(403).json({msg:"No hay productos"})
    } catch (error) {
        return res.status(500).json({msg:error})
    }
    
}

export const getProduct = async (req,res) => {
    try {
        const product = await Product.findByPk(req.params.id)
        if (product){
            return res.status(200).json(product)
        }
        return res.status(403).json({msg:"Producto no existe"})
    } catch (error) {
        return res.status(500).json({msg:error})
    }
    
}

export const updateProduct = async (req,res) => {
    try {
        const product = await Product.update(req.body,{where:{id:req.params.id}})
        if (product[0] == 1) return res.status(200).json({msg:"Producto actualizado"})
        return res.status(403).json({msg:"Producto no existe"})
    } catch (error) {
        return res.status(500).json({msg:error})
    }
}

export const deleteProduct = async (req,res) => {
    try {
        const product = await Product.destroy({where:{id:req.params.id}})
        console.log(product)
        if (product == 1) return res.status(204).json({msg:"Producto eliminado"})
        return res.status(403).json({msg:"Producto no existe"})
    } catch (error) {
        return res.status(500).json({msg:error})
    }
}