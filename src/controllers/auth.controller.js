import User from "../models/User";
import jwt from "jsonwebtoken";
import config from "../config";
import { Role } from "../models/Role";
import { Op } from "sequelize";

export const signIn = async (req,res) => {
    const user = await User.findOne({where:{email:req.body.email}, include:Role})
    if (!user){
        return res.status(400).json({msg:"user not found"})
    }

    const matchPassword = await user.validPassword(req.body.password,user.password)
    if (!matchPassword) return res.status(401).json({token:null,msg:"Invalid password"})

    const token = jwt.sign({id:user.id},config.SECRET, {
        expiresIn: 86400
    })
    return res.status(200).json({token,expiresIn:86400})
}

export const signUp = async (req,res) => {
    try {

        var roles = null;
        if (req.body.roles){
            roles = await Role.findAll({where:{name:{[Op.in]:req.body.roles}}})
            console.log(roles)
        }else{
            roles = await Role.findAll({where:{name:'User'}})
        }
        const user = await User.create(req.body);
        if (user) {
            user.setRoles(roles)
            const token = jwt.sign({id:user.id},config.SECRET, {
                expiresIn: 86400
            })
            return res.status(201).json({token,expiresIn:86400})
        }
        return res.status(403).json({msg:"Usuario no fue creado"})
    } catch (error) {
        return res.status(500).json(error)
    }
    
}