import User from "../models/User";
import { Role } from "../models/Role";
import { Op } from "sequelize";

export const createUser = async (req,res) => {

    try {
        var roles = null;
        if (req.body.roles){
            roles = await Role.findAll({where:{name:{[Op.in]:req.body.roles}}})
        }else{
            roles = await Role.findAll({where:{name:'User'}})
        }
        const user = await User.create(req.body);
        if (user) {
            user.setRoles(roles)
            return res.status(201).json({msg:"Usuario creado",user})
        }
        return res.status(403).json({msg:"Usuario no fue creado"})
    } catch (error) {
        return res.status(500).json(error)
    }


}

export const getUser = async (req,res) => {

    try {
        const user = await User.findByPk(req.params.id, {include:Role});
        if (user) {
            return res.status(201).json({user})
        }
        return res.status(403).json({msg:"Usuario no existe"})
    } catch (error) {
        return res.status(500).json(error)
    }


}

export const getUsers = async (req,res) => {

    try {
        const users = await User.findAll({
            include:Role,
            attributes: {exclude: ['password']}
        })
        if (users.length > 0) {
            return res.status(201).json({users})
        }
        return res.status(403).json({msg:"No hay usuarios"})
    } catch (error) {
        return res.status(500).json(error)
    }


}